import json
import urllib
import logging

import requests
from requests.exceptions import RequestException

import config


class BaseBot:
    """This is a simple template class that can be used to create new bots"""
    def __init__(self, token, bot_name='Telegram'):
        """Constructor

        token -- The token used to access the Telegram api
        bot_name -- The name of the bot (used for logging)"""
        self.bot_name = bot_name
        # Setup logger
        logger = logging.getLogger(self.bot_name)
        logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter(
                '%(asctime)s - %(levelname)s - %(message)s', '%H:%M:%S')
        handler = logging.StreamHandler()
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        self.logger = logger

        self.chat_url = 'https://api.telegram.org/bot{}/'.format(token)
        self.update_url = '{}getUpdates?offset='.format(self.chat_url)
        self.offset = 0
        self.logger.info('Bot started!')

    def get_updates_as_json(self):
        """Query the Telegram API and get new messages if there are any"""
        try:
            self.logger.debug('Starting new query')
            response = requests.get(
                    '{}{}&timeout=60'.format(
                        self.update_url, str(self.offset)))
            return response.json()
        except RequestException as request_exception:
            self.logger.error(
                    'Could not get updates from telegram, \
                            please check connectivity')
            self.logger.debug(request_exception)
            return json.loads('{ "ok": false }')

    def get_last_update_id(self, updates):
        """Returns the last update id of a given list"""
        update_ids = []
        for update in updates['result']:
            update_ids.append(update['update_id'])
        if not update_ids:
            self.logger.debug('Last update ID is 0')
            return 0
        return max(update_ids)
        self.logger.debug('Last update ID is {}'.format())

    def fetch_result(self):
        """This function is used to query results from an api
        (or something alike)"""
        try:
            return requests.get(
                'http://api.icndb.com/jokes/random').json()['value']['joke']
        except RequestException as request_exception:
            self.logger.error('Failed to fetch data from the api')
            self.logger.debug(request_exception)
            return ''

    def reply_all(self, updates):
        """Reply to all clients that texted the bot"""
        for update in updates['result']:
            text = self.fetch_result()
            if not text:
                self.logger.debug('Received an empty text')
                text = 'Nothing to show here'
            chat = update['message']['chat']['id']
            self.send_message(text, chat)

    def send_message(self, text, chat_id):
        """Send message to a specified chat id"""
        text = urllib.parse.quote_plus(text)
        url = '{}sendMessage?text={}&chat_id={}'.format(
            self.chat_url, text, chat_id)
        requests.get(url)

    def serve(self):
        self.logger.info('Starting to listen for messages...')
        while True:
            try:
                updates = self.get_updates_as_json()
                if updates['result']:
                    self.logger.debug('Got an update')
                    self.offset = self.get_last_update_id(updates) + 1
                    self.reply_all(updates)
            except KeyboardInterrupt:
                return


def main():
    bot = BaseBot(config.token)
    bot.serve()


if __name__ == '__main__':
    main()
